This is a README file for the tips algorithm example.

Please see the following link for the confluence page.
https://mymotiv.atlassian.net/wiki/spaces/DAT/pages/510558209/Tips+and+Factoids

"SVD_Tips_Testing.ipynb" -  the original file that works for randomly generated data
"SVD_Tips_Survey.py" -  ingests survey data "Tips - Internal Survey (Part 1).csv" and saves it to a "Tips_part1.csv" 
"data/Tips - Internal Survey (Part 1).csv" - data from Google Form Survey in csv format
"data/Tips_part1.csv" - result of running the first part of "SVD_Tips_Survey.py" to convert data to an easy-to-read format