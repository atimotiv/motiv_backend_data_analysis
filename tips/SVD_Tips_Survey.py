#!/usr/bin/env python3
"""
Created on Mon Aug 13 2018

@author: Anthony Chan

SVD Tips Algorithm

"""

###############################################################################
# tips_dataset_upload
###############################################################################

import pandas as pd
import numpy as np

def extract_google_survey():
	'''
	This section of code is meant to ingest raw data from the google survey and
	convert it into a simple dataframe of 1/0 for like/dislike and easy to read
	column names.


	
	'''

	# reading in data from csv file 
	# https://docs.google.com/spreadsheets/d/1Bpw0FW6AM1rwkxI0lfNmZsXTrvutx_6et3TO6U6TEO8/edit#gid=1897152073
	tips = pd.read_csv("./data/Tips - Internal Survey (Part 1).csv")

	# extracting column Tip IDs that was manually put in as "(ID-X)" at the end of each tip.
	column_names = tips.columns.tolist()[:2] + [c.split("(")[1][:-1] for c in tips.columns.tolist() if c not in ['Timestamp','Username']]

	# creating dictionary to rename each column as an "ID-X" string for column ID
	names_dict = {}
	for i in range(len(column_names)):
	    names_dict[tips.columns.tolist()[i]] = column_names[i]

	# renaming the dataframe columns
	tips.rename(index=str, columns=names_dict, inplace=True)

	# creating binary function, 1 as LIKE and 0 as DISLIKE
	def like_dislike(x):
	    if x == "Like 👍": return 1
	    elif x == "Dislike 👎": return 0
	    else: return x

	tips = tips.applymap(like_dislike)

	# save to csv file in same directory
	tips.to_csv("./data/Tips_part1.csv", index=False)

	return tips
tips = extract_google_survey()


###############################################################################
# SVD_Tips_Survey
###############################################################################
'''
Using Singular-Value Decomposition (SVD) Matrix Decomposition, creates a 
confusion matrix for prediction accuracy based on randomly generated data.
'''


import pandas as pd
import numpy as np
from scipy.sparse.linalg import svds
from sklearn.metrics import r2_score
from sklearn.metrics import confusion_matrix

def get_data(n = 20, k = 10):
    '''
    Setting up DataFrame with 0 and 1 values.
    
    Args:
        n (int): The index length of the DataFrame, aka number of users.
        k (int): The column width of the DataFrame, aka number of tips.

    Returns:
        DataFrame of (n by k) with (0,1) random values, 50-50 split.

    '''
    darr = np.random.randint(low = 0, high = 2, size=(n,k))
    R_df = pd.DataFrame(darr)
    return R_df
# get_data(n = 20, k = 10)


def get_predictions(df, k = 4):
    '''
    Get Matrix Decomposition values from de-meaned DataFrame.
    
    Setting up DataFrame with 0 and 1 values.
    
    Args:
        df (Pandas DataFrame): Input DataFrame.
        k (int): Number of Matrix Decomposition columns to be used. 1 <= k < min(df.shape)

    Returns:
        DataFrame of (n by k) with prediction values in decimal format.
    '''
    if type(df) == type(pd.DataFrame()):
        # de-mean: subtract mean values from each item
        R = df.values
        df_mean = np.mean(R, axis = 1)
        demeaned = R - df_mean.reshape(-1, 1)
        
        # use svd for matrix decomposition
        if k >= 1 and k < df.shape[0]:
            U, sigma, Vt = svds(demeaned, k = k)
            sigma = np.diag(sigma)
            predictions = np.dot(np.dot(U, sigma), Vt) + df_mean.reshape(-1, 1)
            preds_df = pd.DataFrame(predictions, columns = df.columns)

            return preds_df
# get_predictions(df = get_data(n = 20, k = 10), k = 4)


np.random.seed(1234)
def get_sparse_df(preds_df, density = 0.3):
    '''
    Creates a randomly emptied sparse matrix for future recommendations and confusion matrix.
    preds_df data will be taken from get_predictions() and rounded up or down to 0 or 1.
    
    Args:
        preds_df (Pandas DataFrame): The prediction DataFrame resulting from get_predictions().
        density (float): The decimal fraction out of 1.0 of the remaining data in the matrix

    Returns:
        Sparsely dense DataFrame with predictions (0.0, 1.0) or null values (NaN).
    '''
    round_preds_df = preds_df.applymap(lambda x: 1 if x > 0.5 else 0)
    sparse_df = round_preds_df.applymap(lambda x: x if np.random.rand() > density else np.nan) # 0.5
    return sparse_df
# get_sparse_df(preds_df = get_predictions(df = get_data(n = 20, k = 10), k = 4), density = 0.5)


def get_confusion_matrix(y_true, y_pred):
    '''
    Returns confusion matrix based on true values and predicted values.
    
    Implements error handling for y_true == y_pred when it returns 1 number for 100% accuracy instead of 4.
    
    Args:
        y_true (Pandas DataFrame): Original DataFrame.
        y_pred (Pandas DataFrame): Predicted DataFrame.

    Returns:
        Confusion matrix in [[tn, fp],[fn, tp]] format.
    '''
    confusion = confusion_matrix(y_true, y_pred)
    if y_true == y_pred:
        value = confusion[0][0]
        if y_true[0] == 0: # true negative
            return np.array([[value,0],[0,0]])
        elif y_true[0] == 1: # true positive
            return np.array([[0,0],[0,value]])
    return confusion


def recommend_tips(original, predictions, sparse, userID):
    '''
    Calculates R2Score and Confusion Matrix for each row of userID.
    
    Args:
        original (Pandas DataFrame): Original DataFrame.
        predictions (Pandas DataFrame): Predicted DataFrame.
        sparse (Pandas DataFrame): Sparse DataFrame.
        userID (int): Individual row of unique user.

    Returns:
        R2 Score as ([],[])
        Confusion Matrix as np.array([0,0,0,0]).
    '''
    
    # creates row mask for the passed in userID
    user_mask = original.index.values == userID
    user = sparse[user_mask]
    
    # creates column mask for all nan values of a particular user
    column_mask = np.isnan(user).values.ravel()
    
    # initializing variables
    r2, c_mat = 0, 0
    
    # error handles the case where there are no nans in the user column
    # returns tuple with 2 empty lists for R2 Score, and an empty array 2x2 array for Confusion Matrix
    if (sum(column_mask) == 0):
#         return r2, np.array([0,0,0,0])
        return ([],[]), np.array([0,0,0,0])

    # creating 3 dataframes based on row/user mask and column mask
    orig = original.iloc[user_mask, column_mask]
    recommendations = predictions.iloc[user_mask, column_mask]
    recommendations_binary = recommendations.applymap(lambda x: 1 if x > 0.5 else 0)
    
    # creates a tuple for original and recommended list to send back for R2 Score calculation.
    r2 = (np.array(orig)[0].tolist(), np.array(recommendations_binary)[0].tolist())
    
    # calls get_confusion_matrix function that includes error handling
    c_mat = get_confusion_matrix(np.array(orig)[0].tolist(), np.array(recommendations_binary)[0].tolist()).ravel()
    return r2, c_mat


def get_recommendations(R_df, preds_df, sparse_df):
    '''
    Creates string results for R2 Score, Accuracy, and Confusion Matrix.
    
    Args:
        R_df (Pandas DataFrame): Original DataFrame.
        preds_df (Pandas DataFrame): Predicted DataFrame.
        sparse_df (Pandas DataFrame): Sparse DataFrame.

    Returns:
        Dictionary with values for {R2 Score, accuracy, confusion matrix}
    '''
    c_matrix = {'tn': 0, 'fp': 0, 'fn': 0, 'tp': 0}
    orig = []
    recs = []
    
    for i in R_df.index:
        r2, c_mat = recommend_tips(R_df, preds_df, sparse_df, i)
        tn, fp, fn, tp = c_mat
        c_matrix['tn'] += tn
        c_matrix['fp'] += fp
        c_matrix['fn'] += fn
        c_matrix['tp'] += tp
        
        ori, rec = r2
        orig.extend(ori)
        recs.extend(rec)

    r2 = r2_score(orig, recs)
    accuracy = (c_matrix['tn'] + c_matrix['tp']) / float(c_matrix['tn'] + c_matrix['fp'] + c_matrix['fn'] + c_matrix['tp'])
    return {'r2': r2, 'accuracy': accuracy, 'confusion': c_matrix}


# creates randomly generated dataframe and uses it for prediction analysis using SVD
# df = get_data(n = 20, k = 10)
# predictions_df = get_predictions(df = df, k = 4)
# predictions_sparse_df = get_sparse_df(preds_df = predictions_df, density = 0.3)
# get_recommendations(df, predictions_df, predictions_sparse_df)


def random_grid_search(n = 20, k = 10):
    '''
    (Obsolete)
    Creates a grid search looping through k values between 1 and k-1.
    
    Args:
        n (int): number of rows in original DataFrame.
        k (int): used for the k value in SVD
        
    Returns:
        Nothing.
        
    Prints:
        Dictionary containing R2 Score, Accuray, and Confusion Matrix.
    
    '''
    df = get_data(n = n, k = k)
    for s in range(1, k):
        print("k value for SVD:", s)
        predictions_df = get_predictions(df = df, k = s)
        predictions_sparse_df = get_sparse_df(preds_df = predictions_df, density = 0.7)
        print(get_recommendations(df, predictions_df, predictions_sparse_df))
        print()
# random_grid_search(n = 20, k = 10)


print("###############################################################################")
print("SVD_Tips_Survey - with real data")
print("###############################################################################")
'''
Using Singular-Value Decomposition (SVD) Matrix Decomposition, creates a 
confusion matrix for prediction accuracy based on survey data from internal 
Motiv Employees.
'''

# tips = pd.read_csv("./data/Tips_part1.csv")
del tips['Timestamp']
del tips['Username']

# predictions_df = get_predictions(df = tips, k = 4)
# predictions_sparse_df = get_sparse_df(preds_df = predictions_df, density = 0.3)
# get_recommendations(R_df = tips, preds_df = predictions_df, sparse_df = predictions_sparse_df)


def grid_search(df = None, k = 20, density = 0.7):
    '''
    Creates a grid search looping through k values between 1 and k-1.
    
    Args:
        n (int): number of rows in original DataFrame.
        k (int): used for the k value in SVD
        density (float): decimal number between (0,1) to estimate the density of available values in the Sparse DataFrame.
        
    Returns:
        Nothing.
        
    Prints:
        Dictionary containing R2 Score, Accuray, and Confusion Matrix.
    
    '''
    for s in range(1, k):
        print("k value for SVD:", s)
        predictions_df = get_predictions(df = tips, k = s)
        predictions_sparse_df = get_sparse_df(preds_df = predictions_df, density = density)
        print(get_recommendations(R_df = tips, preds_df = predictions_df, sparse_df = predictions_sparse_df))
        print()
grid_search(df = tips, k = tips.shape[1], density = 0.3)


