This is a README file for Weekly and Monthly stats, and Outliers and Data Discrepancies.

Please see the following link for the confluence page.
https://mymotiv.atlassian.net/wiki/spaces/DAT/pages/513146985/Max+Previous+Month+Week+of+Sleep+RHR+Active+Minutes+of+Steps
https://mymotiv.atlassian.net/wiki/spaces/DAT/pages/533627022/Outliers+and+Data+Discrepancies

"summary_and_sleep_matching_Yulia.ipynb" - python notebook that grabs data from MongoDB QA and makes comparisions with Yulia's iOS and Android account.
"summary_and_sleep_matching_Kristina.ipynb" - python notebook that grabs data from MongoDB QA and makes comparisions with Kristina's iOS and Android account.
"summary_and_sleep_matching_Prod.ipynb" - python notebook that grabs data from MongoDB Prod, still in development and only partially works.

Required Libraries: https://github.com/milesrichardson/ParsePy 
Place entire folder named "parse_rest" in the same directory as the python notebooks.